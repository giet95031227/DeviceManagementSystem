﻿namespace MysqlEntityFrameworkProject
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnGetAllDevices = new System.Windows.Forms.Button();
            this.btnGetActiveDevices = new System.Windows.Forms.Button();
            this.btnGetInactiveDevices = new System.Windows.Forms.Button();
            this.btnGetDeviceOnBlacklist = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnGetAllDevices
            // 
            this.btnGetAllDevices.Location = new System.Drawing.Point(40, 24);
            this.btnGetAllDevices.Name = "btnGetAllDevices";
            this.btnGetAllDevices.Size = new System.Drawing.Size(157, 23);
            this.btnGetAllDevices.TabIndex = 0;
            this.btnGetAllDevices.Text = "GetAllDevices";
            this.btnGetAllDevices.UseVisualStyleBackColor = true;
            this.btnGetAllDevices.Click += new System.EventHandler(this.btnGetAllDevices_Click);
            // 
            // btnGetActiveDevices
            // 
            this.btnGetActiveDevices.Location = new System.Drawing.Point(40, 53);
            this.btnGetActiveDevices.Name = "btnGetActiveDevices";
            this.btnGetActiveDevices.Size = new System.Drawing.Size(157, 23);
            this.btnGetActiveDevices.TabIndex = 1;
            this.btnGetActiveDevices.Text = "GetActiveDevices";
            this.btnGetActiveDevices.UseVisualStyleBackColor = true;
            this.btnGetActiveDevices.Click += new System.EventHandler(this.btnGetActiveDevices_Click);
            // 
            // btnGetInactiveDevices
            // 
            this.btnGetInactiveDevices.Location = new System.Drawing.Point(40, 82);
            this.btnGetInactiveDevices.Name = "btnGetInactiveDevices";
            this.btnGetInactiveDevices.Size = new System.Drawing.Size(157, 23);
            this.btnGetInactiveDevices.TabIndex = 2;
            this.btnGetInactiveDevices.Text = "GetInactiveDevices";
            this.btnGetInactiveDevices.UseVisualStyleBackColor = true;
            this.btnGetInactiveDevices.Click += new System.EventHandler(this.btnGetInactiveDevices_Click);
            // 
            // btnGetDeviceOnBlacklist
            // 
            this.btnGetDeviceOnBlacklist.Location = new System.Drawing.Point(40, 111);
            this.btnGetDeviceOnBlacklist.Name = "btnGetDeviceOnBlacklist";
            this.btnGetDeviceOnBlacklist.Size = new System.Drawing.Size(157, 23);
            this.btnGetDeviceOnBlacklist.TabIndex = 3;
            this.btnGetDeviceOnBlacklist.Text = "GetDevicesOnBlacklist";
            this.btnGetDeviceOnBlacklist.UseVisualStyleBackColor = true;
            this.btnGetDeviceOnBlacklist.Click += new System.EventHandler(this.btnGetDeviceOnBlacklist_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.btnGetDeviceOnBlacklist);
            this.Controls.Add(this.btnGetInactiveDevices);
            this.Controls.Add(this.btnGetActiveDevices);
            this.Controls.Add(this.btnGetAllDevices);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnGetAllDevices;
        private System.Windows.Forms.Button btnGetActiveDevices;
        private System.Windows.Forms.Button btnGetInactiveDevices;
        private System.Windows.Forms.Button btnGetDeviceOnBlacklist;
    }
}

