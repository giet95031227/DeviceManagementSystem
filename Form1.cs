﻿using MysqlEntityFrameworkProject.DAL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MysqlEntityFrameworkProject
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnGetAllDevices_Click(object sender, EventArgs e)
        {
            var devices = DeviceHelper.GetAllDevices().ToList();            
        }

        private void btnGetActiveDevices_Click(object sender, EventArgs e)
        {
            var activeDevices = DeviceHelper.GetActiveDevices();
        }

        private void btnGetInactiveDevices_Click(object sender, EventArgs e)
        {
            var inactiveDevices = DeviceHelper.GetInactiveDevices();
        }

        private void btnGetDeviceOnBlacklist_Click(object sender, EventArgs e)
        {
            var devicesOnBlacklist = DeviceHelper.GetDevicesOnBlacklist();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
    }
}
