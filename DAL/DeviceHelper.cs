﻿using System.Linq;
using System.Collections.Generic;

namespace MysqlEntityFrameworkProject.DAL
{
    static class DeviceHelper
    {
        static public List<device> GetAllDevices()
        {
            using (var dbEntities = new rs_cadbEntities())
            {
                return (from d in dbEntities.devices select d).ToList();
            }
        }

        static public List<device> GetActiveDevices()
        {
            using (var dbEntities = new rs_cadbEntities())
            {
                return (from d in dbEntities.devices where d.active_state == 0 select d).ToList();
            }
        }

        static public List<device> GetInactiveDevices()
        {
            using (var dbEntities = new rs_cadbEntities())
            {
                return (from d in dbEntities.devices where d.active_state == 1 select d).ToList();
            }
        }

        static public List<device> GetDevicesOnBlacklist()
        {
            using (var dbEntities = new rs_cadbEntities())
            {
                return (from d in dbEntities.devices
                        join bl in dbEntities.blacklists
                        on d.id equals bl.device_id
                        select d).ToList();
            }
        }
    }
}
